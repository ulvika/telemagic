/**
 * Created by vicki on 17/03/2017.
 */
'use strict';


agentApp.controller('agentList', function($scope, agentData) {
    $scope.data = {};
    $scope.selected = {};
    agentData.getData(function(dataResponse) {
        $scope.searchName='';
        $scope.data = dataResponse.data;
        $scope.selected = $scope.data[0];
        $scope.select = function(item) {
            $scope.selected = item;
        };
        $scope.selectByName = function() {
            var filtered = $scope.data.filter(function (person) {
                var fullname = person.firstName + ' ' + person.lastName;
                return (fullname == $scope.searchName);
            });


            if(filtered.length == 1)
                $scope.selected = filtered[0];
        }
    });

});


