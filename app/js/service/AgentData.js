/**
 * Created by vicki on 18/03/2017.
 */
'use strict';

agentApp.service('agentData', AgentDataService)

function AgentDataService($http, $log){
    var url = 'https://ulvang.info/telemagic/proxy.php?param=agent';
    //delete $http.defaults.headers.common['X-Requested-With'];
    return {
        getData: function(successCallback) {

            $http.get(url).then(successCallback,
                function(errorPayload){
                    $log.log(errorPayload);
                    return '{data: {success: false, message: Error}}'});
        }

    };
}
