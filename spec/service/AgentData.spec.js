/**
 * Created by vicki on 17/03/2017.
 */
describe('agentData ', function(){

    var serviceResponse = '[{"address":"Rønningsvegen 46","domain":"defaultDomain","username":"LeifEinar","city":"Trondheim","agentId":"999999","firstName":"Leif Einar","lastName":"Aune","password":"","phoneNumber":null,"sipUser":"10000","sipHost":"tmgdemo3.telemagic.no","sipSecret":"","roles":["agent","admin","experimental"],"permissions":{"accessOwnRecordings":false,"accessAllJobs":false,"remotelogin":true,"accessRecordings":false,"disableStartManual":false}},{"address":"Ordfører Nielsensvei","domain":"defaultDomain","username":"Tore65","city":"Arendal","agentId":"2000","firstName":"tore","lastName":"Sælen","password":"","phoneNumber":null,"sipUser":"10002","sipHost":"tmgdemo3.dyndns.biz","sipSecret":"","roles":["agent","boffice","admin","experimental"],"permissions":{"accessRecordings":false,"accessOwnRecordings":false,"accessAllJobs":false,"remotelogin":true,"disableStartManual":false}},{"address":"x","domain":"defaultDomain","username":"Tore66","city":"x","agentId":"1234","firstName":"Tore","lastName":"x","password":"","phoneNumber":null,"sipUser":"1003","sipHost":"tmgdemo3.telemagic.no","sipSecret":"","roles":["agent","experimental"],"permissions":{"accessRecordings":false,"accessOwnRecordings":false,"accessAllJobs":false,"remotelogin":true,"disableStartManual":false}},{"address":"x","domain":"defaultDomain","username":"Roger","city":"x","agentId":"1236778","firstName":"Roger","lastName":"Jørgensen","password":"","phoneNumber":"","sipUser":"1017","sipHost":"tmgdemo3.telemagic.no","sipSecret":"","roles":["agent","boffice","bookingowner","admin"],"permissions":{"accessRecordings":false,"accessOwnRecordings":false,"accessAllJobs":false,"remotelogin":false,"disableStartManual":false}},{"address":"x","domain":"defaultDomain","username":"RogerSIP","city":"x","agentId":"9992","firstName":"x","lastName":"x","password":"","phoneNumber":"","sipUser":"1018","sipHost":"tmgdemo3.telemagic.no","sipSecret":"","roles":["agent","admin"],"permissions":{"accessOwnRecordings":false,"accessAllJobs":false,"accessRecordings":false,"remotelogin":false,"disableStartManual":false}},{"address":"","domain":"defaultDomain","username":"Sara","city":"","agentId":"Sara","firstName":"","lastName":"","password":"","phoneNumber":"","sipUser":"1019","sipHost":"tmgdemo3.telemagic.no","sipSecret":"","roles":["agent","boffice","bookingowner","admin"],"permissions":{"accessRecordings":false,"accessOwnRecordings":false,"accessAllJobs":false,"remotelogin":false,"disableStartManual":false}},{"address":"x","domain":"defaultDomain","username":"Dagharald","city":"Skien","agentId":"12367","firstName":"Dag Harald","lastName":"Johansen","password":"","phoneNumber":"","sipUser":"1022","sipHost":"tmgdemo3.dyndns.biz","sipSecret":"","roles":["agent","boffice","bookingowner","admin"],"permissions":{"accessRecordings":false,"accessOwnRecordings":false,"accessAllJobs":true,"remotelogin":true,"disableStartManual":false}},{"address":"","domain":"defaultDomain","username":"janterje2","city":"","agentId":"9997","firstName":"","lastName":"","password":"","phoneNumber":"","sipUser":"1024","sipHost":"tmgdemo3.telemagic.no","sipSecret":"","roles":["agent","admin"],"permissions":{"accessRecordings":false,"accessOwnRecordings":false,"accessAllJobs":false,"remotelogin":true,"disableStartManual":false}},{"address":"Neptunveien 19","domain":"defaultDomain","username":"assignment","city":"Månen","agentId":"23123","firstName":"Ass","lastName":"Ignement","password":"","phoneNumber":"","sipUser":"1000","sipHost":"tmgdemo3.telemagic.no","sipSecret":"","roles":["agent"],"permissions":{"accessRecordings":false,"accessOwnRecordings":false,"accessAllJobs":false,"remotelogin":true,"disableStartManual":false}}]';


    var agentData={};
    var $httpBackend;


    beforeEach(function() {
        module('agentApp');

        inject(function(_agentData_, _$httpBackend_){
            agentData = _agentData_;
            $httpBackend = _$httpBackend_;
        })

    });


    it('should return all agents', function(){
        var data ='';
        $httpBackend.whenGET('proxy.php?param=agent').respond(200, 'success');
        agentData.getData(function(payload){
            data = payload.data;
        });
        $httpBackend.flush();
        expect(data).toEqual('success');
    });
});